import { Category, Intro, Prisma, PrismaClient, User } from '@prisma/client';

import { throws } from 'assert';
import logger from './logger';
import PostModel from '@/models/PostModel';
import CryptoJS from 'crypto-js';

const secret_key = process.env.SECRET;

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
    {
      emit: 'stdout',
      level: 'error',
    },
    {
      emit: 'stdout',
      level: 'info',
    },
    {
      emit: 'stdout',
      level: 'warn',
    },
  ],
});

const findAllPost = async () => {
  logger.info('Fetching Posts list');
  const result = await prisma.post.findMany({
    orderBy: {
      CreatedTime: 'desc',
    },
  });
  if (result) {
    logger.info('', result.length);
    return result;
  }
};

// USER
const verifyLogin = async (UserId, Password): Promise<User> => {
  const encryptedPassword = CryptoJS.HmacSHA256(
    Password,
    secret_key
  ).toString();
  return await prisma.user.findFirst({
    where: {
      AND: [{ UserId }, { Password: encryptedPassword }],
    },
  });
};

const findUserById = async (UserId: string): Promise<User> => {
  return await prisma.user.findFirst({
    where: { UserId },
  });
};

const listUser = async (): Promise<User[]> => {
  return await prisma.user.findMany();
};

const createUser = async (user): Promise<User> => {
  return await prisma.user.create({ data: user });
};
// end of User

// Category
const findAllCategory = async (): Promise<Category[]> => {
  return await prisma.category.findMany();
};

const createCategory = async (category: Category[]) => {
  return await Promise.all(
    category.map(async (category) => {
      try {
        return await prisma.category.create({ data: category });
      } catch (e) {
        console.log(e);
      }
    })
  );
};

const deleteAllCategories = async () => {
  return await prisma.category.deleteMany();
};
// End of Category

// Intro
const findAllIntro = async (): Promise<Intro[]> => {
  return await prisma.intro.findMany();
};

const newIntro = async (intro: Intro): Promise<Intro> => {
  return await prisma.intro.create({ data: intro });
};

const useNewIntro = async (id: number): Promise<Intro> => {
  return await prisma.intro.update({
    where: { Id: id - 1 },
    data: { Status: 0 },
  });
};

const findIntro = async (): Promise<Intro> => {
  return await prisma.intro.findFirst({
    where: { Status: 1 },
  });
};

const countIntro = async (): Promise<number> => {
  return await prisma.intro.count();
};

export default {
  POST: { findAllPost },
  USER: { findUserById, listUser, createUser, verifyLogin },
  CATEGORY: { findAllCategory, createCategory, deleteAllCategories },
  INTRO: { findAllIntro, newIntro, useNewIntro, findIntro, countIntro },
};
