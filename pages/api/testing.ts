import query from '@/utils/query';
import { NextApiRequest, NextApiResponse } from 'next';

const testing = async (req: NextApiRequest, res: NextApiResponse) => {
  const result = query.findAllPost();
  if (result) {
    console.log((await result).length);
    res.json(result);
  }
};

export default testing;
