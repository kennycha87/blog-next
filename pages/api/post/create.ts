import logger from '@/utils/logger';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const create: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  if (req.method !== 'PUT') {
    logger.info('Method is not allow!');
    res.status(401).send({ message: 'Method is not allow!' });
  } else {
  }
};

export default create;
