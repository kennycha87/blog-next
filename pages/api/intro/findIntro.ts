import logger from '@/utils/logger';
import query from '@/utils/query';
import { NextApiRequest, NextApiResponse } from 'next';

const findIntro = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'GET') {
    logger.info('{} mehtod is not allow', req.method);
  } else {
    const result = await query.INTRO.findIntro();
    if (result) {
      res.status(200).json({
        status: 200,
        data: result,
      });
    } else {
      res.status(200).json({
        status: 204,
        message: 'No record is found',
      });
    }
  }
};

export default findIntro;
