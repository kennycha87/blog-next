import logger from '@/utils/logger';
import query from '@/utils/query';
import { Intro } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';

const updateIntro = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  if (req.method !== 'PATCH') {
    logger.info('{} is not allow', req.method.toUpperCase());
    res.status(401).send({ messgae: 'Method is not allow' });
  } else {
    const createResult = await query.INTRO.newIntro(req.body);
    createResult
      ? postCreation(res, createResult)
      : res.status(500).send({
          message: 'Fail to insert Intro, please contact system admin',
        });
  }
};

const postCreation = async (
  res: NextApiResponse,
  result: Intro
): Promise<void> => {
  const count = await query.INTRO.countIntro();
  if (count < 1) {
    res.status(201).json({
      status: 201,
      message: 'The first Introduction has been created',
      data: result,
    });
  } else {
    const dsiableOldIntro = await query.INTRO.useNewIntro(result.Id);
    if (dsiableOldIntro) {
      res.status(201).json({
        status: 201,
        messgae: 'Create Intro success',
        data: result,
      });
    } else {
      res.status(500).send({ message: 'Fail to disable old Intro' });
    }
  }
};

export default updateIntro;
