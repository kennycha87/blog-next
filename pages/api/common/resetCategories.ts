import logger from '@/utils/logger';
import query from '@/utils/query';
import { NextApiRequest, NextApiResponse } from 'next';

const resetCategories = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'DELETE') {
    logger.info('Invaild request method');
    res.status(401).send({ message: 'Method is not allow!' });
  } else {
    try {
      const result = await query.CATEGORY.deleteAllCategories();
      logger.debug(JSON.stringify(result));
      res.status(200).json(result);
    } catch (e) {
      res.status(500).send({ message: e });
    }
  }
};

export default resetCategories;
