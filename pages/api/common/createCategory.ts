import logger from '@/utils/logger';
import query from '@/utils/query';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const createCategory: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  if (req.method !== 'POST') {
    res.status(401).send({ message: 'Method is not allow' });
  } else {
    try {
      const result = await query.CATEGORY.createCategory(req.body);
      if (result) {
        console.log(result);
        res.status(201).json(result);
      }
    } catch (e) {
      logger.error(e);
      res.status(500).send({ message: 'Unable to save' });
    }
  }
};

export default createCategory;
