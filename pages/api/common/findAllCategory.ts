import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';
import query from '@/utils/query';
import { Category } from '@prisma/client';

const category: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const result: Category[] = await query.CATEGORY.findAllCategory();
  if (result.length > 0) {
    res.status(200).json(result);
  } else {
    res.status(200).json({ message: 'No record found!' });
  }
};

export default category;
