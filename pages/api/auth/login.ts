import React from 'react';
import { sign } from 'jsonwebtoken';
import { serialize } from 'cookie';
import CryptJS from 'crypto-js';
import { prisma, User } from '@prisma/client';
import query from '@/utils/query';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const secret = process.env.SECRET;

const login: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const { UserId, Password } = req.body;
  const verifyLogin = await query.USER.verifyLogin(UserId, Password);
  console.log(verifyLogin);
  if (verifyLogin) {
    res.json({
      status: 200,
      token: sign(
        {
          expiresIn: '15m',
          UserId,
        },
        secret
      ),
    });
  } else {
    res.status(200).send({
      status: 401,
      message: 'Invalid credentials!',
    });
  }
};

export default login;
