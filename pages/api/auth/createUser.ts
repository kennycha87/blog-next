import logger from '@/utils/logger';
import query from '@/utils/query';
import CryptoJS from 'crypto-js';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const createUser: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  if (req.method !== 'POST') {
    res.status(405).send({ message: 'Method not allows' });
  } else {
    logger.info(`Create user with parameter: ${JSON.stringify(req.body.user)}`);
    const { UserId, Password } = req.body.user;

    // check if User Id is exist
    const userExists = await query.USER.findUserById(UserId);
    if (userExists) {
      // return message
      res.status(400).send({ message: 'UserId has been used' });
    } else {
      // encrypt passwowrd with HmacSHA256
      const encryptedPassword = CryptoJS.HmacSHA256(
        Password,
        process.env.SECRET
      ).toString();

      // insert into DB
      try {
        const result = await query.USER.createUser({
          UserId,
          Password: encryptedPassword,
        });
        res.status(201).json({ message: `${req.body.user.UserId} is created` });
      } catch (e) {
        logger.error(e);
        res.status(500).send({
          message:
            'Unable to insert new user into database, please check system log',
        });
      }
    }
  }
};

export default createUser;
