import query from '@/utils/query';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';

const listUser: NextApiHandler = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const result = await query.USER.listUser();
  console.log(result);
  if (result.length > 0) {
    res.status(200).json({ result });
  } else {
    res.status(501).json({ message: 'No users found' });
  }
};

export default listUser;
