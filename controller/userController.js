const jwt = require('jsonwebtoken');
const queriesloader = require('../Utils/queriesloader');
const sqlHandler = require('../Utils/mySqlHandler');
const CryptoJS = require('crypto-js');

const secret_key = process.env.SECRET_KEY;
const queries = queriesloader('User');

const userLogin = async (req, res, next) => {
  // query the database and get the result
  try {
    const user = await verifyUser(req.body);
    const processedResult = await JSON.parse(JSON.stringify(user[0]));
    const token = await jwt.sign(processedResult, secret_key, {
      expiresIn: '15m',
    });
    res.json({ token });
  } catch (error) {
    console.log(error);
  }
};

const createUser = async (req, res, next) => {
  const hashPassword = CryptoJS.HmacSHA256(
    req.body.Password,
    secret_key
  ).toString();
  try {
    const result = sqlHandler(queries.createUser, {
      ...req.body,
      Password: hashPassword,
    });
    if (result) {
      res.status(201).json({ msg: 'User Created' });
    }
  } catch (error) {
    console.log(error);
  }
};

const verifyUser = async (user) => {
  try {
    const results = await sqlHandler(queries.verifyUser, [
      user.UserId,
      CryptoJS.HmacSHA256(user.Password, secret_key).toString(),
    ]);
    if (results) {
      console.log('Verify User: success');
      return results;
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  userLogin,
  createUser,
};
