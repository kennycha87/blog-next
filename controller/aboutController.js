const queriesloader = require('../Utils/queriesloader');
const sqlHandler = require('../Utils/mySqlHandler');
const { imageUpload } = require('../Utils/uploadHandler');
const { v4: UUIDv4 } = require('uuid');
const multer = require('multer');

const queries = queriesloader('User');

const updateAbout = async (req, res) => {
  try {
    const result = await sqlHandler(queries.updateAbout, req.body.Describes);
    if (result) {
      res.json({ msg: 'About description has been updated successful' });
    }
  } catch (error) {
    console.log(err);
    res.status(405);
  }
};

const updateProfilePicture = async (req, res) => {
  try {
    const result = await sqlHandler(queries.updateAboutImage, req.body);
    if (result) {
      res.json({ msg: 'Profile picture hasb been updated to database' });
    }
  } catch (error) {
    res.json({ error: 'update database fail!' });
  }
};

const uploadPicture = async (req, res) => {
  const ext = req.file.detectedFileExtension;

  if (ext === '.jpg' || ext === '.jpeg' || ext === '.png') {
    const fileName = UUIDv4() + ext;

    // save the file to static folder using fileupload handler
    imageUpload(req.file, fileName).then((result) => {
      if (result.err) {
        res.status(500).json();
      } else {
        const query = sqlHandler(queries.updateAboutImage, {
          Picture: fileName,
        }).catch((err) => {
          res.status(500).json();
        });
        query.then((r) => {
          res.json({ msg: 'Post has been created', fileName });
        });
      }
    });
  } else {
    res.status(405);
  }
};

module.exports = {
  updateAbout,
  uploadPicture,
  updateProfilePicture,
};
