const queriesloader = require('../Utils/queriesloader');
const sqlHandler = require('../Utils/mySqlHandler');
const { imageUpload } = require('../Utils/uploadHandler');
const moment = require('moment');

const queries = queriesloader('Post');
const commentQueries = queriesloader('Comment');

const getPosts = async (req, res) => {
  try {
    const posts = await sqlHandler(queries.getPosts);
    const comments = await sqlHandler(commentQueries.findComments);
    if (posts && comments) {
      console.log(posts);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json();
  }
};

const getPostById = async (req, res) => {
  if (req.body) {
    try {
      const result = await sqlHandler(queries.getPostById, { Id: body.PostId });
      if (result) {
        console.log(result);
      }
    } catch (error) {
      res.status(500);
    }
  }
};

const createPost = async (req, res) => {
  console.log('create post');
  try {
    if (req.body) {
      const result = await sqlHandler(queries.createPost, req.body);
      if (result) {
        console.log('POST: post has been created');
        console.log(result);
        res.json({
          msg: `Post #${result.insertId} has been created`,
          insertId: result.insertId,
        });
      }
    }
  } catch (error) {
    console.log(error);
    req.status(500);
  }
};

const getCategories = (req, res) => {
  const result = sqlHandler(queries.getCategorie).catch((err) =>
    console.log(err)
  );
  result.then((result) => {
    res.json(result.data);
  });
};

const updatePost = async (req, res) => {
  console.log('edit post');
  console.log(req.body);
  try {
    const result = await sqlHandler(queries.updatePost, [
      {
        ...req.body,
        LastModified: moment(Date.now()).format(),
      },
      req.body.Id,
    ]);
    console.log(result);
    if (result.affectedRows > 0) {
      console.log('Post has been updated');
      res.json({ msg: 'Post has been updated' });
    }
  } catch (error) {
    console.log(error);
  }
};

const deletePost = (req, res) => {
  const result = sqlHandler(queries.deletePost, req.body.Id);
  result
    .then((result) => {
      res.json({ msg: 'Post has been Deleted' });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json();
    });
};

module.exports = {
  getPosts,
  getCategories,
  createPost,
  deletePost,
  updatePost,
  getPostById,
};
