import { useSelector, useDispatch } from 'react-redux';
import { NextRouter, useRouter } from 'next/router';
import Link from 'next/link';
import CategoryBar from './CategoryBar';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import SearchField from './SearchField';
import IconsBar from './IconsBar';
import { rootState } from '../../store';
import { postActions } from '../../store/slices/post-slice';
import { NextComponentType } from 'next';
import { Dispatch } from '@reduxjs/toolkit';

const HeaderBar: NextComponentType = () => {
  const dispatch: Dispatch = useDispatch();

  const isLiggedIn: Boolean = useSelector(
    (state: rootState) => state.auth.isLoggedIn
  );

  const edit: Boolean = useSelector((state: rootState) => state.post.edit);

  const router: NextRouter = useRouter();

  const newPostHandler: any = async () => {
    if (edit === true) {
      await dispatch(postActions.setEdit());
    }
    await router.push('/NewPost');
  };

  return (
    <>
      <AppBar
        position='fixed'
        color='secondary'
        style={{ boxShadow: 'none', marginBottom: '1rem', minHeight: '64px' }}
      >
        <Toolbar
          disableGutters
          sx={{ paddingLeft: '1rem', justifyContent: 'space-between' }}
        >
          {isLiggedIn && (
            <Button
              variant='contained'
              size='small'
              sx={{
                marginRight: 1,
              }}
              onClick={newPostHandler}
            >
              New
            </Button>
          )}
          <Typography
            variant='h5'
            noWrap
            component='div'
            sx={{
              flexGrow: 1,
              display: { xs: 'none', sm: 'block' },
            }}
          >
            <Link href='/'>Kenny Leung Blog</Link>
          </Typography>

          {/* search field */}
          <SearchField />
          <IconsBar />
        </Toolbar>
        <CategoryBar />
      </AppBar>
    </>
  );
};

export default HeaderBar;
