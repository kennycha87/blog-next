-- CreateTable
CREATE TABLE "User" (
    "Id" TEXT NOT NULL PRIMARY KEY,
    "UserId" TEXT NOT NULL,
    "Password" TEXT NOT NULL,
    "Gender" INTEGER,
    "Birthday" DATETIME,
    "Email" TEXT,
    "TelZone" INTEGER,
    "Tel" INTEGER,
    "CreatedTime" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "CreatedUser" TEXT,
    "LastModifiedDate" DATETIME,
    "LastModifiedUser" TEXT,
    "Remarks" TEXT
);

-- CreateTable
CREATE TABLE "Post" (
    "Id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "Title" TEXT NOT NULL,
    "Category" INTEGER NOT NULL,
    "Tags" TEXT,
    "Description" TEXT NOT NULL,
    "Contents" TEXT NOT NULL,
    "Status" INTEGER NOT NULL DEFAULT 1,
    "CreatedTime" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "CreatedUser" INTEGER,
    "LastModified" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "LastModifiedUser" INTEGER
);

-- CreateTable
CREATE TABLE "Category" (
    "Id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "Name" TEXT NOT NULL,
    "Status" INTEGER NOT NULL DEFAULT 1,
    "CreatedTime" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "CreatedUser" INTEGER,
    "LastModified" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "LastModifiedUser" INTEGER,
    "Remarks" TEXT
);

-- CreateTable
CREATE TABLE "Intro" (
    "Id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "Description" TEXT,
    "Title" TEXT,
    "Status" INTEGER NOT NULL DEFAULT 1,
    "Picture" TEXT
);

-- CreateTable
CREATE TABLE "Comment" (
    "Id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "PostId" INTEGER NOT NULL,
    "Name" TEXT NOT NULL,
    "Email" TEXT NOT NULL,
    "Comment" TEXT NOT NULL,
    "CreatedTime" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- CreateIndex
CREATE UNIQUE INDEX "User_UserId_key" ON "User"("UserId");
