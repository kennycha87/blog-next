import { NextResponse } from 'next/server';
import { verify } from 'jsonwebtoken';

const secret = process.env.SECRET;

const middleware = (req) => {
  const { cookies } = req;

  const jwt = cookies.OursiteJWT;

  const url = req.url;

  if (url.includes('/about')) {
    if (jwt == undefined) {
      return NextResponse.redirect('/login');
    }
    try {
      verify(jwt, secret);
      return NextResponse.next();
    } catch (e) {
      return NextResponse.redirect('/login');
    }
  }
  return NextResponse.next();
};

export default middleware;
